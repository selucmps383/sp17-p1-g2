﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Phaseinventorytracker.Models;
using Microsoft.AspNetCore.Identity;

namespace Phaseinventorytracker.Models
{
    public static class DbInitializer
    {
        public static void Initialize(PhaseinventorytrackerContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.User.Any())
            {
                return;   // DB has been seeded
            }

            PasswordHasher<string> pw = new PasswordHasher<string>();
            var users = new User[]
            {
            new User{
                Username = "admin",
                FirstName ="Adi",
                LastName ="Min",
                Password = pw.HashPassword("admin", "selu2017")
        },
            new User{
                Username = "jbob",
                FirstName ="John",
                LastName ="Bob",
                Password = pw.HashPassword("jbob", "password")
            },

            };
            foreach (User u in users)
            {
                context.User.Add(u);
            }
            context.SaveChanges();
        }
    }
}
