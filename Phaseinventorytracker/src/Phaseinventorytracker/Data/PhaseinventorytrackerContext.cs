﻿using Microsoft.EntityFrameworkCore;

namespace Phaseinventorytracker.Models
{
    public class PhaseinventorytrackerContext : DbContext
    {
        public PhaseinventorytrackerContext(DbContextOptions<PhaseinventorytrackerContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }

        public DbSet<InventoryItem> InventoryItem { get; set; }
    }
}