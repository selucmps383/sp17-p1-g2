﻿using System;
using System.Linq;
using Phaseinventorytracker.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Phaseinventorytracker.Models;

namespace Phaseinventorytracker
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }


        public IConfigurationRoot Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMvc();
            //            services.AddAuthorization(options =>
            //                {
            //                    options.AddPolicy("admin", policy =>
            //                    {
            //                        policy.RequireClaim("role", "admin");
            //                    });
            //
            //                    options.AddPolicy("user", policy =>
            //                    {
            //                        policy.RequireClaim("role", "admin");
            //                        policy.RequireClaim("role", "user");
            //                    });
            //                });

            services.AddDistributedMemoryCache();
            services.AddSession();
            services.AddDbContext<PhaseinventorytrackerContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("PhaseinventorytrackerContext")));

            services.AddDbContext<PhaseinventorytrackerContext>(options =>
            {
                var connectionString = Configuration.GetConnectionString("PhaseinventorytrackerContext");
                options.UseSqlServer(connectionString);
            });

            services.AddIdentity<IdentityUser, IdentityRole>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, PhaseinventorytrackerContext context)

        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();
            app.UseSession();

            app.UseIdentity();

            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = "MyCookieMiddlewareInstance",
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                LoginPath = new PathString("/Login/Login"),
                AccessDeniedPath = new PathString("/Login/Login")
            });

            //            app.UseMvc(routes =>
            //            {
            //                routes.MapRoute(
            //                    name: "default",
            //                    template: "{controller=Home}/{action=Index}/{id?}");
            //            });
            // SeedData.Initialize(app.ApplicationServices);
            app.UseMvcWithDefaultRoute();
            DbInitializer.Initialize(context);
        }
    }
}