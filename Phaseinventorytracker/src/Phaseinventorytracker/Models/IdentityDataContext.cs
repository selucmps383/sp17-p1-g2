﻿using Microsoft.EntityFrameworkCore;

namespace Phaseinventorytracker.Models
{
    public class IdentityDataContext : DbContext
    {
        public IdentityDataContext(DbContextOptions<IdentityDataContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}