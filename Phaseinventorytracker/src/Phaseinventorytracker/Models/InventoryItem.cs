﻿namespace Phaseinventorytracker.Models
{
    public class InventoryItem
    {
        public int Id { get; set; }
        public int CreatedByUserId { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }
    }
}