﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phaseinventorytracker.Models;

namespace Phaseinventorytracker.Controllers
{
    public class HomeController : Controller
    {
        private PhaseinventorytrackerContext _context;
        private readonly IAuthorizationService _authz;


        public HomeController(PhaseinventorytrackerContext context, IAuthorizationService authz)
        {
            _context = context;
            _authz = authz;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}