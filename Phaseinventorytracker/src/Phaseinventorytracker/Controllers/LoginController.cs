using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Phaseinventorytracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Phaseinventorytracker.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly PhaseinventorytrackerContext _context;

        public LoginController(PhaseinventorytrackerContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View(_context.User.ToList());
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([Bind("Username, Password")]User user)
        {
            PasswordHasher<string> pw = new PasswordHasher<string>();
            PasswordVerificationResult result = new PasswordVerificationResult();

            var usr = _context.User.Single(u => u.Username == user.Username);
            var userPassword = (from at in _context.User where at.Username == user.Username select at).Select(a => a.Password).Single();

            result = pw.VerifyHashedPassword(user.Username, userPassword, user.Password);

            if (result == PasswordVerificationResult.Success || result == PasswordVerificationResult.SuccessRehashNeeded)
            {
                string userRole;
                if (user.Username == "admin")
                {
                    userRole = "admin";
                }
                else
                {
                    userRole = "user";
                }

                var claims = new List<Claim>
                 {
                    new Claim("Username",user.Username),
                    new Claim(ClaimTypes.Role, userRole)
                 };
                var userIdentity = new ClaimsIdentity(claims);
                var userPrincipal = new ClaimsPrincipal(userIdentity);

                await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", userPrincipal,
                    new AuthenticationProperties
                    {
                        //ExpiresUtc = DateTime.UtcNow.AddMinutes(10),
                        IsPersistent = false,
                        AllowRefresh = false
                    });
                HttpContext.Session.SetString("Id", usr.Id.ToString());
                HttpContext.Session.SetString("Username", usr.Username.ToString());
                return RedirectToAction("Welcome");
            }
            else
            {
                ModelState.AddModelError("", "Username or Password is incorect.");
            }
            return View();
        }

        public IActionResult Welcome()
        {
            if (HttpContext.Session.GetString("Id") != null)
            {
                ViewBag.Username = HttpContext.Session.GetString("Username");
                // Redirect to the welcome page (Need to also check if the session has the credentials in the view page too)
                return RedirectToAction("Index", "InventoryItems");
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            await HttpContext.Authentication.SignOutAsync("MyCookieMiddlewareInstance");
            return RedirectToAction("Login");
        }
    }
}